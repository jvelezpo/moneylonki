Rails.application.routes.draw do

  get 'settings/index'

  get 'spend_imports/new'

  post 'spend_imports', to: 'spend_imports#create'

  resources :categories
  resources :spends do
    collection { get :year }
    collection { get :daily }
    collection { get :monthly }
  end
  devise_for :users
  get 'pages/index'

  get '*unmatched_route', to: 'pages#error'

  devise_scope :user do
    authenticated :user do
      root to: "spends#index", as: :authenticated_root
    end

    unauthenticated do
      root to: "pages#index"
    end
  end

end
