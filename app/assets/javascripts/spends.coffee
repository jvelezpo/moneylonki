# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

weekdays = new Array(7)
weekdays[0] = "Sun"
weekdays[1] = "Mon"
weekdays[2] = "Tue"
weekdays[3] = "Wed"
weekdays[4] = "Thu"
weekdays[5] = "Fri"
weekdays[6] = "Sat"

ready = ->

  unless $("#spends_chart") is `undefined`
    $.getJSON $("#spends_chart").data("url"), (data) ->
      $("#last_year_chart").show()
      Morris.Line
        element: 'spends_chart'
        data: data
        xkey: 'when_at'
        ykeys: ['amount']
        labels: ['Amount']
        preUnits: '$'
        xLabels: 'month'

  unless $("#week_spends_chart") is `undefined`
    $.getJSON $("#week_spends_chart").data("url"), (data) ->
      $("#last_week_chart").show()
      Morris.Area
        element: 'week_spends_chart'
        data: data
        xkey: 'when_at'
        ykeys: ['amount']
        labels: ['Amount']
        preUnits: '$'
        xLabels: 'day'
        xLabelFormat: oelo = (d) ->
          weekdays[d.getDay()] + ' ' + d.getDate()

  unless $("#month_spends_chart") is `undefined`
    $.getJSON $("#month_spends_chart").data("url"), (data) ->
      $("#last_month_chart").show()
      Morris.Line
        element: 'month_spends_chart'
        data: data
        xkey: 'when_at'
        ykeys: ['amount']
        labels: ['Amount']
        preUnits: '$'
        xLabels: 'day'

$(document).ready ready
$(document).on "page:load", ready

$(document).on "ready page:change", ->
  $(".tag-tooltip").tooltip()

