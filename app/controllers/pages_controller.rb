class PagesController < ApplicationController

  before_action :authenticate_user!, except: [:index]

  def index
  end

  def error
    redirect_to root_path, flash: {danger: 'Error 404. Resource does not exists.' }
  end
end
