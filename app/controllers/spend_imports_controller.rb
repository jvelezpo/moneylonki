class SpendImportsController < ApplicationController

  def new
    @spend_import = SpendImport.new
  end

  def create
    @spend_import = SpendImport.new(params[:spend_import])

    begin
      if @spend_import.save(current_user)
        redirect_to root_path, flash: {success: 'Expenses imported.' }
      else
        render :new
      end
    rescue
      redirect_to spend_imports_new_path, flash: {danger: 'Expenses not imported.' }
    end

  end

end
