class SpendsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_spend, only: [:show, :edit, :update, :destroy]

  # GET /spends
  # GET /spends.json
  def index
    @spends = current_user.spends.all.order(when_at: :desc, created_at: :desc)
    @spends_months = @spends.group_by { |t| t.when_at.beginning_of_month }
    @sum = []


    @spends_months.each do |month, spends|
      sum = 0
      pie = {}

      spends.each do |spend|
        amount = spend.amount
        sum += amount
        if pie[spend.category.name].nil?
          pie[spend.category.name] = amount
        else
          pie[spend.category.name] += amount
        end

      end
      pie['sum'] = sum
      @sum.push(pie)
    end

    respond_to do |format|
      format.html
      format.csv { send_data @spends.order(:when_at).to_csv(headers: true), filename: "expenses-#{Date.today}.csv" }
      format.xls #{ send_data @spends.to_csv(col_sep: "\t") }
    end
  end

  # GET /spends/1
  # GET /spends/1.json
  def show
  end

  # GET /spends/new
  def new
    @spend = current_user.spends.build
    @spend.when_at = Time.now
  end

  # GET /spends/1/edit
  def edit
  end

  # POST /spends
  # POST /spends.json
  def create
    @spend = current_user.spends.build(spend_params)

    respond_to do |format|
      if @spend.save
        format.html { redirect_to spends_path, flash: {success: 'Spend was successfully created.' }}
        format.json { render :show, status: :created, location: @spend }
      else
        format.html { render :new }
        format.json { render json: @spend.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /spends/1
  # PATCH/PUT /spends/1.json
  def update
    respond_to do |format|
      if @spend.update(spend_params)
        format.html { redirect_to spends_path, flash: {success: 'Spend was successfully updated.' }}
        format.json { render :show, status: :ok, location: @spend }
      else
        format.html { render :edit }
        format.json { render json: @spend.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /spends/1
  # DELETE /spends/1.json
  def destroy
    @spend.destroy
    respond_to do |format|
      format.html { redirect_to spends_url, flash: {success: 'Spend was successfully destroyed.' }}
      format.json { head :no_content }
    end
  end

  def year
    start = 1.year.ago.to_date.beginning_of_month>>1
    spends_by_day = Spend.total_grouped_by_month(start, current_user.id)

    @data = []

    while true
      break if start > Date.today.end_of_month
      if !spends_by_day[start.month].nil?

        amount = 0
        spends_by_day[start.month].each do |a|
          amount += a.amount
        end
        a = {
            when_at: start.strftime('%Y-%m'),
            amount: amount
        }
        @data.push(a)
      end
      start += 1.month
    end
    @data
  end

  def daily
    @data = from_spends_chart_data
  end

  def monthly
    month = params['month']
    if !month.nil?
      begin
        @data = from_spends_chart_data(month.to_date.beginning_of_month)
      rescue
        @data = []
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_spend
    @spend = Spend.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def spend_params

    p = params.require(:spend).permit(:amount, :description, :when_at, :category)
    p['when_at'] = Date.strptime(p['when_at'], '%m/%d/%Y').strftime('%Y-%m-%d')
    if p['category'] != ''
      p['category'] = Category.find(p['category'].to_i)
      p
    end
  end

  def from_spends_chart_data(start = 1.weeks.ago+1.day)
    spends_by_day = Spend.total_grouped_by_day(start, current_user.id)
    (start.to_date..Date.today).map do |date|
      {
          when_at: date,
          amount: spends_by_day[date].nil? ? 0 : (spends_by_day[date].first.try(:amount) || 0)
      }
    end
  end

end
