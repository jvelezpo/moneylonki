json.array!(@spends) do |spend|
  json.extract! spend, :id, :amount, :description, :when
  json.url spend_url(spend, format: :json)
end
