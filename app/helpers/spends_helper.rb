module SpendsHelper

  def spend_line_chart(url, id)
    content_tag :div, nil, data: { url: url }, id: id
  end

end
