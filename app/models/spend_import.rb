class SpendImport

  extend ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :file

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def save(current_user)
    ActiveRecord::Base.transaction do
      @imported_expenses ||= load_imported_expenses(current_user)


      if @imported_expenses.map(&:valid?).all?
        @imported_expenses.each(&:save!)
        true
      else
        @imported_expenses.each_with_index do |spend, index|
          spend.errors.full_messages.each do |message|
            errors.add :base, "Row #{index+2}: #{message}"
          end
        end
        raise ActiveRecord::Rollback
        false
      end
    end
  end

  def load_imported_expenses(current_user)
    case File.extname(file.original_filename)
      when ".csv" then
        CSV.foreach(file.path, headers: true).map do |row|

          Spend.transaction do
            current_user.spends.destroy_all
            
            line = row.to_hash
            category_name = line['category']
            
            spend = Spend.new
            spend.attributes = line.except('category')
            spend.user_id = current_user.id

            if !row["category"].nil? 
              category = current_user.categories.find_by_name(category_name)
              if category.nil?
                ca = {}
                ca['name'] = category_name
                ca = current_user.categories.build(ca)
                if ca.save
                  spend.category_id = ca.id  
                else
                  raise "Unknown category: #{category_name}"
                end
              else
                spend.category_id = category.id
              end
            end

            spend
          end
        end
      else
        raise "Unknown file type: #{file.original_filename}"
    end
  end

end