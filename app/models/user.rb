class User < ActiveRecord::Base

  after_create :create_basic_categories

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :spends
  has_many :categories

  def create_basic_categories
    self.categories.build(name: 'Food')
    self.categories.build(name: 'Fuck')
    self.categories.build(name: 'Other')
    self.categories.build(name: 'Transportation')
    self.categories.build(name: 'Drinks')
    self.categories.build(name: 'Sports')
    self.categories.build(name: 'Health')
    self.categories.build(name: 'Phone')
    self.categories.build(name: 'Gas')
    self.categories.build(name: 'Movies')
  end
end
