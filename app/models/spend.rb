class Spend < ActiveRecord::Base

  belongs_to :user
  belongs_to :category

  validates :category, presence: true
  validates :amount, presence: true
  validates :when_at, presence: true

  def self.total_grouped_by_day(start, user_id)
    spends = where(when_at: start.beginning_of_day..Time.zone.now).where("user_id = (?)", user_id)
    spends = spends.group("date(when_at)")
    spends = spends.select('date(when_at) as when_at, sum(amount) as amount')
    spends.group_by { |o| o.when_at.to_date }
  end

  def self.total_grouped_by_month(start, user_id)
    spends = where(when_at: start.beginning_of_day..Time.zone.now).where("user_id = (?)", user_id)
    spends = spends.group("date(when_at)")
    spends = spends.select('date(when_at) as when_at, sum(amount) as amount')
    spends.group_by { |o| o.when_at.to_date.month }
  end

  def self.to_csv(options = {})
    attributes = %w{amount description when_at created_at updated_at}

    CSV.generate(options) do |csv|
      attributes.push("category")
      csv << attributes
      attributes.pop()
      all.each do |spend|
         line = attributes.map{ |attr| spend.send(attr) }
         line.push(spend.category.name)
         csv << line
      end
    end
  end

end