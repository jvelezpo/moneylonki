class Category < ActiveRecord::Base

  belongs_to :user
  has_one :spend, dependent: :restrict_with_exception
end
