class AddUserIdToSpend < ActiveRecord::Migration
  def change
    add_column :spends, :user_id, :integer
  end
end
