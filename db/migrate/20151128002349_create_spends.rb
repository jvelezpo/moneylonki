class CreateSpends < ActiveRecord::Migration
  def change
    create_table :spends do |t|
      t.decimal :amount
      t.string :description
      t.datetime :when_at

      t.timestamps null: false
    end
  end
end
