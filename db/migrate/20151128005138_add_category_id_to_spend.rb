class AddCategoryIdToSpend < ActiveRecord::Migration
  def change
    add_column :spends, :category_id, :integer
  end
end
